from django.contrib import admin
from django.urls import path, include
from reviews import views

app_name = 'reviews'

urlpatterns = [
    path('reviews/', views.ReviewList.as_view(), name='review_list'),
    path('reviews/<int:pk>/', views.ReviewDetail.as_view(), name='review_detail'),
]
