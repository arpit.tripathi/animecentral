from django.db import models
from animedata.models import Anime
from django.utils import timezone
from django.contrib.auth.models import User

# Create your models here.
class Review(models.Model):
    id = models.AutoField(primary_key=True)
    anime = models.ForeignKey("animedata.Anime", related_name='reviews', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='reviews', on_delete=models.CASCADE)
    content = models.TextField()
    created = models.DateTimeField(editable=False)
    modified = models.DateTimeField()
    is_modified = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created = timezone.now()
        else:
            self.is_modified = True
        self.modified = timezone.now()
        return super(Review, self).save(*args, **kwargs)