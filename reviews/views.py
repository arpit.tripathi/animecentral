from django.shortcuts import render
from .models import Review
from .serializers import ReviewSerializer
from rest_framework import generics, filters
from django.db.models.functions import Cast
from rest_framework import permissions
from .permissions import IsOwnerOrReadOnly

# Create your views here.

class ReviewList(generics.ListCreateAPIView):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class ReviewDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly,)