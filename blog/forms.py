from django import forms

from .models import Post

class PostForm(forms.ModelForm):
    title = forms.CharField(
        max_length=30,
        widget=forms.TextInput(
            attrs={
                'placeholder': 'Write your title here'
            }
        )
    )

    text = forms.CharField(
        max_length=2000,
        widget=forms.Textarea(attrs={'placeholder': 'Write your Post here'})
    )

    class Meta:
        model = Post
        fields = ('title', 'text',)