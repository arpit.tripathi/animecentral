from django.shortcuts import render

# Create your views here.
from django.shortcuts import redirect
from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from rest_framework import generics
from django.contrib.auth.models import User
from .serializers import UserSerializer, BlogsSerializer
from front_end.models import UserProfile
from django.contrib.auth.decorators import login_required
from .forms import PostForm
from .models import Post
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse

def post_list(request):

    posts = Post.objects.values("id","title","text","created_date","published_date","author__username","author").order_by("-published_date")
    # user = {}
    # for post in posts:
    #     print("-------------")
    #     print(post)
    #     image_url = UserProfile.objects.values('profile_pic').get(user=post['author'])
    #
    #     user[post['author']] = image_url['profile_pic']
    # print(user)
    return render(request, 'blog/post_list.html', {'posts': posts})


def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'blog/post_detail.html', {'post': post})


def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('blog:post_list')
    else:
        form = PostForm()
    return render(request, 'blog/post_edit.html', {'forms': form})


def post_edit(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('blog:post_list')
    else:
        form = PostForm(instance=post)
    return render(request, 'blog/post_edit.html', {'forms': form})

#delete a review
@login_required
def delete_post(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if post.author == request.user:
        print('working')
        Post.objects.filter(id=pk).delete()
        return HttpResponseRedirect(reverse('front_end:user-profile'))
    return HttpResponseRedirect(reverse('front_end:user-profile'))

#-----------------------------API----------------------------------------#

class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class UserDetails(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class Blogs_list(generics.ListAPIView):
    queryset = Post.objects.all()
    serializer_class = BlogsSerializer

class Blogs_details(generics.RetrieveUpdateDestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = BlogsSerializer


