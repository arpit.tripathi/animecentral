from rest_framework import serializers
from .models import Post
from django.contrib.auth.models import User


class BlogsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = "__all__"

class UserSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = User
        fields = ('username',)