from django.urls import path

from . import views

app_name = 'blog'

urlpatterns = [
    path('', views.post_list, name='post_list'),
    path('post/<int:pk>/', views.post_detail, name='post_detail'),
    path('post/new', views.post_new, name='post_new'),
    path('post/<int:pk>/edit/', views.post_edit, name='post_edit'),
    path('post/delete/<int:pk>/', views.delete_post, name='delete-post'),
    path('user/', views.UserList.as_view()),
    path('user/<int:pk>', views.UserDetails.as_view()),
    path('blog_details', views.Blogs_list.as_view(), name='blog-detail'),
    path('blog_details/<int:pk>', views.Blogs_details.as_view(), name='blog-detail'),
]
