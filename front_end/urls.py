from django.contrib import admin
from django.urls import path, include
from .views import anime_detail
from django.conf.urls import url
from . import views

app_name = 'front_end'

urlpatterns = [
    path('anime/<int:pk>/', views.anime_detail, name='anime-detail'),
    path('anime/', views.anime_list),
    path('anime/search/', views.anime_search, name='anime-search'),
    path('anime/list/', views.anime_list, name='anime-list'),
    path('anime/reviews/', views.anime_review, name='anime-review'),
    path('anime/reviews/<int:pk>/', views.post_review, name='post-review'),
    path('anime/reviews/delete/<int:pk>/', views.delete_review, name='delete-review'),
    path('profile/reviews/edit/<int:pk>/', views.edit_review, name='edit-review'),
    path('', views.anime_carousal, name='home'),
    path('register/', views.register, name='register'),
    path('login/', views.user_login, name='login'),
    path('logout/', views.user_logout, name='logout'),
    path('profile/', views.user_profile, name='user-profile'),
    path('profile/watch_list/', views.watch_list, name='watch-list'),
    path('profile/<int:pk>/', views.profile, name='profile'),
    ]
