from django.shortcuts import render, get_object_or_404
import json
import urllib
from urllib.request import urlopen
# for authentication and login we are importing the following
from django.shortcuts import render, redirect
from front_end.forms import SignUpForm, PostReviewForm
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth import authenticate,login,logout
from animedata.models import Anime, Animelist
from blog.models import Post
from django.core.validators import URLValidator
import re
from django.core.exceptions import ValidationError
from reviews.models import Review
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.utils import timezone

# ---------------------------------------------------------------
# base html when we enter the address    http:127.0.0.1:8000
def home(request):
    return render(request,'front_end/base.html')


# user registration
def register(request):
    registered = False
    if request.method == 'POST':
        user_form = SignUpForm(data=request.POST)
        if user_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()

            registered = True
        else:
            print(user_form.errors)
    else:
        user_form = SignUpForm()

    return render(request,'front_end/register.html',{'user_form':user_form,'registered':registered})


# user login page
def user_login(request):
    if request.method== 'POST':
        print("i am inside post")
        username = request.POST.get('username')
        password = request.POST.get('password')
        print(username,password)
        user = authenticate(username=username,password=password)
        print(user)
        if user:
            if user:
                    login(request,user)
                    return redirect('front_end:home')
            else:
                return HttpResponse('User doesnot exists')
        else:
            message = "user name and password is invalid"
            return render(request,'front_end/login.html',{'context':message})
    else:
        return render(request,'front_end/login.html')

# user profile page
def user_profile(request):
    user_data = json.load(urlopen("http://127.0.0.1:8000/api/users/{}".format(request.user.id)))
    reviews = Review.objects.values('anime__title', 'anime__image_url', 'content', 'created', 'id').filter(user_id=request.user.id).order_by('-created')
    posts = Post.objects.filter(author_id=request.user.id).order_by('-published_date')
    return render(request, 'front_end/profile.html', {'user_data': user_data, 'reviews': reviews, 'posts': posts, 'user_profile': True})

def profile(request, pk):
    user_data = json.load(urlopen("http://127.0.0.1:8000/api/users/{}".format(pk)))
    reviews = Review.objects.values('anime__title', 'anime__image_url', 'content', 'created').filter(user_id=pk).order_by('-created')
    posts = Post.objects.filter(author_id=request.user.id).order_by('-published_date')
    return render(request, 'front_end/profile.html', {'user_data': user_data, 'reviews': reviews, 'posts': posts, 'user_profile': False})

# after login user redirecting to mainpage page
def mainpage(request):
    print('i am in mainpage')
    return render(request, 'front_end/userpage.html')


# logout redirects to home page
@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('front_end:home'))


# anime detail
def anime_detail(request, pk):
    anime_reviews = Review.objects.values('content', 'created', 'user__username').filter(anime_id=pk).order_by('-created')
    anime_data = json.load(urlopen("http://127.0.0.1:8000/api/anime/{}".format(pk)))
    characters_data = json.load(urlopen("https://api.jikan.moe/v3/anime/{}/characters_staff".format(pk)))
    return render(request, 'front_end/anime_detail.html', {'anime_detail' : anime_data, 'characters_data': characters_data, 'reviews': anime_reviews})

# anime list
def anime_list(request):
    anime_data = json.load(urlopen("http://127.0.0.1:8000/api/anime/"))
    try:
        search_variable = request.POST['url']
        anime_data = json.load(urlopen("{}".format(search_variable)))
    except:
        pass
    return render(request, 'front_end/anime_list.html', {'anime_data' : anime_data,})

def anime_search(request):
    try:
        search_variable = request.GET['search']
        search_variable = search_variable.replace(' ', '+')
        anime_data = json.load(urlopen("http://127.0.0.1:8000/api/anime?search={}".format(search_variable)))
    except:
        pass
    try:
        search_variable = request.POST['url']
        anime_data = json.load(urlopen("{}".format(search_variable)))
    except:
        pass
    
    return render(request, 'front_end/anime_search.html', {'anime_data': anime_data})


def anime_carousal(request):
    anime = Anime.objects.all()
    anime_10 = []
    for anime_item in anime:
        regex = re.compile(
            r'^(?:http|ftp)s?://'  # http:// or https://
            r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
            r'localhost|'  # localhost...
            r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}|'  # ...or ipv4
            r'\[?[A-F0-9]*:[A-F0-9:]+\]?)'  # ...or ipv6
            r'(?::\d+)?'  # optional port
            r'(?:/?|[/?]\S+)$', re.IGNORECASE)
        if(regex.search(anime_item.image_url) != None):
            anime_10.append(anime_item)


    return render(request, 'front_end/carousal.html', {'anime': anime_10[:10]})

# fetch all reviews   
def anime_review(request):
    reviews = Review.objects.values('anime__title', 'anime__image_url', 'content', 'created', 'user__username', 'user__id').order_by('-created')
    paginator = Paginator(reviews, 10) # Show 25 contacts per page
    page = request.GET.get('page')
    contacts = paginator.get_page(page)
    print(contacts.object_list)
    return render(request, 'front_end/anime_reviews.html', {'reviews': contacts})

@login_required
def post_review(request, pk):
    registered = False
    review = Review.objects.get(user=request.user.id, anime_id=pk)
    anime = get_object_or_404(Anime, anime_id=pk)
    if not review and request.method == 'POST':
        post_form = PostReviewForm(data=request.POST)
        if post_form.is_valid():
            post = post_form.save(commit=False)
            post.user = request.user
            post.modified = timezone.now()
            post.anime_id = pk
            post.save()
            registered = True
        else:
            print(post_form.errors)
    elif review:
        return render(request,'front_end/post_review.html', {'reviewed':True, 'review_id': review.id})
    user_form = PostReviewForm()
    return render(request,'front_end/post_review.html',{'user_form':user_form,'registered':registered})

#delete a review
@login_required
def delete_review(request, pk):
    review = get_object_or_404(Review, pk=pk)
    if review.user == request.user:
        print('working')
        Review.objects.filter(id=pk).delete()
        return HttpResponseRedirect(reverse('front_end:user-profile'))
    return HttpResponseRedirect(reverse('front_end:user-profile'))

@login_required
def edit_review(request, pk):
    post = get_object_or_404(Review, pk=pk)
    registered = False
    if request.method == "POST":
        form = PostReviewForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.user = request.user
            post.modified = timezone.now()
            post.save()
            registered = True
            return redirect('front_end:user-profile')
        else:
            return redirect('404.html')
    else:
        user_form = PostReviewForm(instance=post)
    return render(request,'front_end/post_review.html',{'user_form':user_form,'registered':registered})

# watch_list
@login_required
def watch_list(request):
    username =request.user
    anime_list = Animelist.objects.values('anime__title', 'anime__image_url', 'user__username', 'status').filter(user_id=request.user.id).order_by('anime__title')
    paginator = Paginator(anime_list, 10) # Show 25 contacts per page
    page = request.GET.get('page')
    contacts = paginator.get_page(page)
    return render(request,'front_end/watch_list.html',{'anime_list':contacts, 'username': username})    
