from django import forms
from django.contrib.auth.models import User
from reviews.models import Review

# creating model form that wil connect to user model
class SignUpForm(forms.ModelForm):
    password= forms.CharField(widget=forms.PasswordInput)
    confirm_password=forms.CharField(widget=forms.PasswordInput())
    class Meta():
        model = User
        fields = ('username','email','password')

# checking password ,confirm password are same
    def clean(self):
        cleaned_data = super(SignUpForm, self).clean()
        password = cleaned_data.get("password")
        confirm_password = cleaned_data.get("confirm_password")

        if password != confirm_password:
            raise forms.ValidationError(
                "password and confirm_password does not match"
            )

class PostReviewForm(forms.ModelForm):
    class Meta():
        model = Review
        fields = ('content',)

