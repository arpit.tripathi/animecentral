from django.shortcuts import render
from animedata.models import Anime
from animedata.serializers import AnimeSerializer, UserSerializer
from rest_framework import generics, filters
from django.db.models.functions import Cast
from django.contrib.auth.models import User



class AnimeList(generics.ListAPIView):
    queryset = Anime.objects.exclude(rank__in=['', '0'])
    serializer_class = AnimeSerializer
    filter_backends = (filters.OrderingFilter, filters.SearchFilter,)
    search_fields = ('title',)
    filter_fields = ('series_type',)
    ordering_fields = ('rank', 'members', 'score', )


class AnimeDetail(generics.RetrieveAPIView):
    queryset = Anime.objects.all()
    serializer_class = AnimeSerializer


class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer