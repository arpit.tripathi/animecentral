from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('anime/', views.AnimeList.as_view(), name='anime-list'),
    path('anime/<int:pk>/', views.AnimeDetail.as_view(), name='anime-detail'),
    path('users/', views.UserList.as_view(), name='user-list'),
    path('users/<int:pk>/', views.UserDetail.as_view(), name='user-detail'),
]
