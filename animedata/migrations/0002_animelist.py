# Generated by Django 2.1.2 on 2018-10-19 11:42

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('animedata', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Animelist',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(blank=True, max_length=15, null=True)),
                ('anime', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='anime_list', to='animedata.Anime')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='anime_list', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
