from django.core.management.base import BaseCommand
from django.conf import settings
import csv
from animecentral.animedata.models import Anime
# from animedata.models import Anime
from django.core.management.base import BaseCommand
from django.db import transaction
import datetime
import json, ast

def import_anime(anime_csv):
    with open(anime_csv) as f:
        anime_reader = csv.DictReader(f)
        print("adding anime file to db...")
        for anime in anime_reader:
            aired = ast.literal_eval(anime['aired'])
            aired_from = '0001-01-01'
            aired_to = '0001-01-01'
            if aired['from'] is not None:
                aired_from = aired['from']
            if aired['to'] is not None:
                aired_to = aired['to']
            Anime(
            anime_id = anime['anime_id'],
            title = anime['title'],
            title_english = anime['title_english'],
            title_synonyms = anime['title_synonyms'],
            image_url = anime['image_url'],
            series_type = anime['type'],
            source = anime['source'],
            episodes = anime['episodes'],
            status = anime['status'],
            airing = anime['airing'],
            aired_string = anime['aired_string'],
            start_date = datetime.datetime.strptime(aired_from, '%Y-%m-%d'),
            end_date = datetime.datetime.strptime(aired_to, '%Y-%m-%d'),
            score = anime['score'],
            scored_by = anime['scored_by'],
            rank = anime['rank'],
            popularity = anime['popularity'],
            members = anime['members'],
            favorites = anime['favorites'],
            background = anime['background'],
            premiered = anime['premiered'],
            broadcast = anime['broadcast'],
            related = anime['related'],
            producer = anime['producer'],
            licensor = anime['licensor'],
            studio = anime['studio'],
            genre = anime['genre'],
            opening_theme = anime['opening_theme'],
            ending_theme = anime['ending_theme']
            ).save()
        print("anime file is added to db")


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument(
            "file_path",
            nargs=1,
            type=str,
        )

    

    def handle(self,*args,**options):
        anime_csv = options["file_path"][0]
        with transaction.atomic():
            import_anime(anime_csv)

       
   