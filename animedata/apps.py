from django.apps import AppConfig


class AnimedataConfig(AppConfig):
    name = 'animedata'
