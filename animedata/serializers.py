from rest_framework import serializers
from animedata.models import Anime
from reviews.models import Review
from reviews.serializers import ReviewSerializer
from django.contrib.auth.models import User


class AnimeSerializer(serializers.HyperlinkedModelSerializer):
    reviews = ReviewSerializer(many=True)
    class Meta:
        model = Anime
        fields = ('url', 'anime_id', 'title', 'title_english', 'title_synonyms', 'image_url', 'series_type', 'source', 'episodes', 'status', 'airing', 'aired_string', 'start_date', 'end_date', 'duration', 'rating', 'score', 'scored_by', 'rank', 'popularity', 'members', 'favorites', 'background', 'premiered', 'broadcast', 'related', 'producer', 'licensor', 'studio', 'genre', 'opening_theme', 'ending_theme', 'reviews')


class UserSerializer(serializers.ModelSerializer):
    reviews = ReviewSerializer(many=True)
    class Meta:
        model = User
        fields = ('username', 'reviews')